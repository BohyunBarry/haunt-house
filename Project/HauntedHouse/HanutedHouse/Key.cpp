

#include "Key.h"



Key::Key(string n, string c) : Object(n, c)
{
}

string Key::examine() const
{
	return "The Key is " + getColor() + " in Colour and reads \"" + getName() + "\" seems to be a key to a room somewhere.";
}


string Key::open()
{
	return "How to open a key!?";
}

string Key::useWith(Object& object) {
	if (object.getName() == getName()) 
	{ 
		object.unlock(true);
		return "You no longer have any use for this key, keep it as a memento!";
	}
	return "The Key must be wrong for this door!";
}

string Key::unlock(bool o)
{
	return "No! You must unlock something else with the key, not the key itself...";
}

Key::~Key()
{
}
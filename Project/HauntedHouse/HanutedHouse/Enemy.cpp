

#include "Enemy.h"

using std::to_string;


Enemy::Enemy() : Object()
{
	damage = 0;
}

Enemy::Enemy(string n, string c, int dmg) : Object(n,c)
{
	damage = dmg;
}

string Enemy::unlock(const bool o)
{
	return "The " + getName() + " has nothing to unlock";
}

string Enemy::examine() const
{
	return "this looks like a " + getName() + " you cannot reason with it";
}

string Enemy::open()
{
	return "The " + getName() + " has nothing to open";
}

string Enemy::useWith(Object& object)
{
	return "How to use the enemy with something?!";
}

void Enemy::setDamage(const int& dmg)
{
	damage = dmg;
}

int Enemy::getDamage() const
{
	return damage;
}

string Enemy::attack() const
{
	return "The " + getName() + " Lunges at you, you take " + to_string(damage) + "damage";
}

Enemy::~Enemy()
{
}

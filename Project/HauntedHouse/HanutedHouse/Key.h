

#pragma once
#include "Object.h"
// defines what can be in a key, it unlocks some door classes
class Key : public Object
{
public:
	Key(string n, string c);
	// implement virtual functions
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	~Key();
};

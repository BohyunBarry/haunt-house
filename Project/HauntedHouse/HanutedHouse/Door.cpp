

#include "Door.h"

//defines a door empty for the room class
Door::Door() : Object("No Door", "No Color")
{
	unlocked = false;
}

Door::Door(string n, string c, bool u) : Object(n, c)
{
	unlocked = u;
}

string Door::unlock(bool o)
{
	unlocked = o;
	if (unlocked == true)
	{
		return "You unlocked the " + getName();
	}
	return "This door cannot be unlocked like this!?";
}

string Door::open()
{
	if (unlocked == true)
	{
		return "You open the door and enter the " + getName().substr(0, getName().find_last_of(' ')) + "...";
	}
	return "The Door is Locked, there is a carving of a " + getName().substr(0, getName().find_last_of(' ')) + " on the door handle...";
}

string Door::examine() const
{
	if (unlocked == false)
	{
		return "The door is " + getColor() + " in color and you notice a carving of a " + getName().substr(0,getName().find_last_of(' ')) + " on the door handle, it appears locked, maybe it needs a key...?";
	}
	return "The door is " + getColor() + " in color and you notice a carving of a " + getName().substr(0, getName().find_last_of(' ')) + " on the door handle, it appears to be unlocked";
}

string Door::useWith(Object& object)
{
	return "How can you use a door on that!? Try using something ON the door...";
}

bool Door::isUnlocked() const
{
	return unlocked;
}

Door::~Door()
{
}

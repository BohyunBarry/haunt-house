

#pragma once
#include "Object.h"
#include <string>

using std::string;

class Enemy : public Object
{
private:
	int damage;
public:
	Enemy();
	Enemy(string n, string c, int dmg);
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	void setDamage(const int& dmg);
	int getDamage() const;
	string attack() const;
	~Enemy();
};


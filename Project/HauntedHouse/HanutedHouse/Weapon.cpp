

#include "Weapon.h"


Weapon::Weapon() : Object()
{
	power = 0;
}

Weapon::Weapon(string n, string c, int p) : Object (n,c)
{
	power = p;
}

string Weapon::unlock(const bool o)
{
	return "Cannot unlock the weapon!";
}

string Weapon::examine() const
{
	return "This " + getName() + " looks dangerous against enemies";
}

string Weapon::open()
{
	return "You cannot open this " + getName() + ", It does not do that!!";
}

string Weapon::useWith(Object& object)
{
	if (typeid(object).name() == "Enemy")
	{
		return "You used the weapon on the " + object.getName() + "...";
	}
	return "You cannot use the " + getName() + " On this, only enemies!!";
}

void Weapon::setPower(const int& p)
{
	power = p;
}

int Weapon::getPower() const
{
	return power;
}

Weapon::~Weapon()
{
}


#include "Note.h"



Note::Note(string n, string c, string d) : Object(n,c)
{
	description = d;
}

string Note::examine() const
{
	return "This is a note, it's title is: " + getName();
}

string Note::open()
{
	return "The note reads: \n" + description;
}

string Note::unlock(bool o)
{
	return "You can't unlock the note..";
}

string Note::useWith(Object& object)
{
	return "I don't think that will be of much use...";
}

string Note::getDescription() const
{
	return description;
}

void Note::setDescription(const string& d)
{
	description = d;
}

Note::~Note()
{
}

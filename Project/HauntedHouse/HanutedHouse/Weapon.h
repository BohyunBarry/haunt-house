

#pragma once
#include "Object.h"
#include <string>

using std::string;

class Weapon : public Object
{
private:
	int power;
public:
	Weapon();
	Weapon(string n, string c, int p);
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	void setPower(const int& p);
	int getPower() const;
	~Weapon();
};


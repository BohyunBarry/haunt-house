

#pragma once
#include "Object.h"
// defines what can be in a Door, a key must be accepted to unlock the door with matching name of this door
class Door : public Object
{
private:
	bool unlocked;
public:
	Door();
	Door(string n, string c, bool u);
	// implement virtual functions
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	bool isUnlocked() const;
	~Door();
};


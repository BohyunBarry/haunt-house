

#pragma once
#include <string>

using std::string;
// general class for all objects to inherit from - base methods common to all objects in our games
class Object
{
private:
	// private functions and variables go here
	string name;
	string color;
public:
	// need default constructor to declare list of objects for the game inventory
	Object();
	// virtual functions define to be implemented by derived classes of this class
	Object(string n, string c);
	virtual string unlock(const bool o);
	virtual string examine() const;
	virtual string open();
	virtual string useWith(Object& object);
	string getName() const;
	void setName(const string& n);
	void setColor(const string& c);
	string getColor() const;
	virtual ~Object();
};


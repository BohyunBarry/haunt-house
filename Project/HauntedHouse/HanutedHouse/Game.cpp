

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <regex>
#include "Object.h"
#include "Door.h"
#include "Note.h"
#include "Key.h"
#include "Room.h"
#include "Vase.h"
#include "FileRead.h"
#include "Container.h"
#include "Weapon.h"
#include "Enemy.h"
#include "Engine.h"


using std::vector;
using std::cout;
using std::endl;
using std::cin;
using std::regex;
using std::ifstream;

// prototyping main functions are done in the Engine.h header file and defined in Engine.cpp c++ file



// include your file for game and constructor of FileRead object to directory to your file

int main()
{
	// create the ENEMY, WEAPON and CONTAINER classes
	// need to add ENEMY, WEAPON and CONTAINER lists to ROOM class

	// creating the games inventory, file read input and current room and string for input
	vector<Object> inventory;
	FileRead file("information.txt");
	vector<Room> rooms = createRooms("rooms.txt");
	// default room is the main hall
	Room current = rooms[0];
	string input = "";
	string roomName = "";
	bool alive = true;
	// read into for the game in
	cout << file.readRoom(0) << endl;
	// read main hall into the game using room object - current
	cout << file.readRoom(current.getRoomNumber()) << endl;

	// player will play until the get outside the house
	while (current.getName() != "outside" && alive)
	{
		
		cout << "what would you like to do?" << endl;
		getline(cin, input);
		transform(input.begin(), input.end(), input.begin(), ::tolower);
		// after converting their text entry into lowercase, we check what they entered
		if (input == "controls")
		{
			cout << file.readRoom(0) << endl;
		}
		else if (input == "inventory")
		{
			showInventory(inventory);
		}
		else if (input.substr(0,4) == "move")
		{

			vector<Door> doors = current.getDoors();
			bool proceed = false;
			// move and a direction checks door is open and then moves and reprints new room description
			roomName = getRoomName(current, input);

			if (roomName == "not valid")
			{
				cout << "No door in this direction" <<  endl;
			}

			// check correct direction door is open or not
			if (input.substr(5) == "north")
			{
				if (doors[0].getName() != "no door")
				{
					cout << doors[0].open() << endl;
					proceed = doors[0].isUnlocked();
				}
				
			}
			else if (input.substr(5) == "east")
			{
				if (doors[1].getName() != "no door")
				{
					cout << doors[1].open() << endl;
					proceed = doors[1].isUnlocked();
				}
			}
			else if (input.substr(5) == "south")
			{
				if (doors[2].getName() != "no door")
				{
					cout << doors[2].open() << endl;
					proceed = doors[2].isUnlocked();
				}
			}
			else if (input.substr(5) == "west")
			{
				if (doors[3].getName() != "no door")
				{
					cout << doors[3].open() << endl;
					proceed = doors[3].isUnlocked();
				}
			}
			
			// using door state to confirm if to change room
			if (proceed)
			{
				// here we create the new room and then show it to them
				current = setRoom(rooms, current, roomName.substr(0,roomName.find_first_of(' ')));
				if (current.getName() == "invalid")
				{
					cout << "Failed to load the room, something went wrong" << endl;
				}
				else
				{
					cout << file.readRoom(current.getRoomNumber()) << endl;
				}
				
			}
		}
		else if (input.substr(0,7) == "examine")
		{
			examineObjects(input, current, inventory);
			
		}

		// check if something is attempted to be taken
		else if (input.substr(0,4) == "take")
		{
			takeObjects(input, current, inventory);
			
		}
		// for useing items, wrong choice can kill player with the wild bear
		else if (input.substr(0,3) == "use")
		{
			useObjects(input, current, inventory, alive);
		}
		else if (input.substr(0,4) == "open" || input.substr(0,5) == "smash")
		{
			openObjects(input, current, inventory);
		}
		else if (input == "describe room")
		{
			cout << file.readRoom(current.getRoomNumber()) << endl;
		}
		else
		{
			cout << "I cannot understand what you said" << endl;
		}
	}

	// game over when while loop is over

	return 0;
}


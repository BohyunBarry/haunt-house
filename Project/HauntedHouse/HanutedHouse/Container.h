

#pragma once
#include "Object.h"
#include <vector>

using std::vector;

// defines what other objects can be held in the container
class Container : public Object
{
private:
	bool opened;
	vector<Object> content;
public:
	Container();
	Container(string n, string c, vector<Object> objects);
	// implement virtual functions
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	string smash();
	void setContents(const vector<Object>& objects);
	vector<Object> getContents() const;
	~Container();
};


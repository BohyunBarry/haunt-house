

#pragma once


vector<Room> createRooms(const string& dir);
void roomChange(FileRead& file, Room& r);
Room setRoom(vector<Room>& rooms, Room& current, string& loc);
string getRoomName(Room& current, string& input);
void showInventory(vector<Object>& inventory);
void examineObjects(string& input, Room& current, vector<Object>& inventory);
void takeObjects(string& input, Room& current, vector<Object>& inventory);
void useObjects(string& input, Room& current, vector<Object>& inventory, bool& alive);
void openObjects(string& input, Room& current, vector<Object>& inventory);
bool checkInventory(const string& search, const vector<Object>& inventory);
int selectInventory(vector<Object>& inventory, string& item);
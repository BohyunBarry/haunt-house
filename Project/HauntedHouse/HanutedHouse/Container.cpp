

#include "Container.h"



Container::Container() : Object()
{
	opened = false;
}

Container::Container(string n, string c, vector<Object> objects) : Object(n,c)
{
	opened = false;
	content = objects;
}

// implement virtual functions
string Container::unlock(const bool o)
{
	return "Cannot Unlock this";
}

string Container::examine() const
{
	return "This is a "+ getName() + ", it looks like it contains something, try opening it...";
}

string Container::open()
{
	opened = true;
	return "You open it up...";
}

string Container::smash()
{
	opened = true;
	return "You smashed it open...";
}

string Container::useWith(Object& object)
{
	return "You cannot use it like this!";
}

void Container::setContents(const vector<Object>& objects)
{
	content = objects;
}

vector<Object> Container::getContents() const
{
	if (opened)
	{
		return content;
	}
	vector<Object> empty;
	return empty;
}


Container::~Container()
{

}

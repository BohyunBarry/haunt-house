
#include "Room.h"



Room::Room(string n, string c, int no, vector<Note> ns, vector<Key> ks, vector<Container> cs, vector<Door> ds, vector<Enemy> es, vector<Weapon> ws) : Object(n,c)
{
	roomNumber = no;
	notes = ns;
	keys = ks;
	containers = cs;
	doors = ds;
	enemies = es;
	weapons = ws;
}

string Room::unlock(const bool o)
{
	return "Choose a key and use on a door to unlock something";
}

string Room::examine() const
{
	return "This room looks like a " + getName() + ", it is " + getColor() + " in color...\n";
}

void Room::setRoomNumber(const int& no)
{
	roomNumber = no;
}

int Room::getRoomNumber() const
{
	return roomNumber;
}

string Room::open()
{
	return "You cannot open the room, you must select a valid direction with a door to open that instead";
}

string Room::useWith(Object& object)
{
	return "You would use a key on a door intead of the room itself";
}

bool Room::hasDoor(const int& dir) const
{
	if (doors[dir].getName() != "No Door")
	{
		return true;
	}
	return false;
}

string Room::getDoorName(const int& dir) const
{
	return doors[dir].getName();
}

void Room::setNotes(const vector<Note>& ns)
{
	notes = ns;
}

void Room::setKeys(const vector<Key>& ks)
{
	keys = ks;
}

void Room::setContainers(const vector<Container>& cs)
{
	containers = cs;
}

vector<Note> Room::getNotes() const
{
	return notes;
}

vector<Key> Room::getKeys() const
{
	return keys;
}

vector<Container> Room::getContainers() const
{
	return containers;
}

void Room::setDoors(const vector<Door>& ds)
{
	doors = ds;
}

vector<Door> Room::getDoors() const
{
	return doors;
}

void Room::setEnemies(const vector<Enemy>& es)
{
	enemies = es;
}

vector<Enemy> Room::getEnemies() const
{
	return enemies;
}

void Room::setWeapons(const vector<Weapon>& ws)
{
	weapons = ws;
}

vector<Weapon> Room::getWeapons() const
{
	return weapons;
}


Room::~Room()
{
}

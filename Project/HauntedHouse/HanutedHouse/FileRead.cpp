

#include "FileRead.h"
#include <fstream>

using std::ifstream;
using std::stringbuf;



FileRead::FileRead(string dir)
{
	directory = dir;
}
// assume 20 lines of text per room, because of intro
string FileRead::readRoom(const int& r) const
{
	string text, description = "";
	string prev = "prev";
	ifstream in(directory);


	// first read to point we don't want to save
	for (int i = 0; i < (r * 21); i++)
	{
		getline(in, text);
	}
	for (int j = 0; j < 21; j++)
	{
		getline(in, text);
		if (text == "" && prev == "")
		{
			break;
		}
		prev = text;
		// check removes seconds blank line on text after reading in an empty line
		if (j == 0)
		{
			description += text;
		}
		else
		{
			description += '\n' + text;
		}
		
	}
	return description;
}

FileRead::~FileRead()
{
}

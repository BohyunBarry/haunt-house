

#pragma once
#include "Object.h"
// defines what can be in a note, it is a letter with text, we need description for the content
class Note : public Object
{
private:
	string description;
public:
	Note(string n, string c, string d);
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	string getDescription() const;
	void setDescription(const string& d);
	~Note();
};




#pragma once
#include <vector>
#include "Key.h"
#include "Note.h"
#include "Door.h"
#include "Vase.h"
#include "Container.h"
#include "Enemy.h"
#include "Weapon.h"

using std::vector;

// defines what can be in a room, all other objects that we make in our game are contained within rooms
class Room : public Object
{
private:
	int roomNumber;
	// a room can have many different items depending on the game being made
	vector<Note> notes;
	vector<Key> keys;
	vector<Container> containers;
	vector<Enemy> enemies;
	vector<Weapon> weapons;
	// maximum of four doors on a screen, North - 0, East 1, South 2, West 3 - index to check if door exists
	vector<Door> doors;
public:
	Room(string n, string c, int no, vector<Note> ns, vector<Key> ks, vector<Container> cs, vector<Door> ds, vector<Enemy> es, vector<Weapon> ws);
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	// checking a door exists in that direction 0 = north, 1 = east, 2 = south, 3 = west, index of vector for number
	bool hasDoor(const int& dir) const;
	// setter for the door description, no need to get as done in the examine method
	void setRoomNumber(const int& n);
	void setNotes(const vector<Note>& ns);
	void setKeys(const vector<Key>& ks);
	void setContainers(const vector<Container>& cs);
	void setDoors(const vector<Door>& ds);
	void setEnemies(const vector<Enemy>& es);
	void setWeapons(const vector<Weapon>& ws);
	vector<Note> getNotes() const;
	vector<Key> getKeys() const;
	vector<Container> getContainers() const;
	vector<Door> getDoors() const;
	vector<Enemy> getEnemies() const;
	vector<Weapon> getWeapons() const;
	int getRoomNumber() const;
	string Room::getDoorName(const int& dir) const;
	~Room();
};


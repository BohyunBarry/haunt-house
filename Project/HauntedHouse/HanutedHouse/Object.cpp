

#include "Object.h"

Object::Object()
{
	name = "Blank";
	color = "Blank";
}

Object::Object(string n, string c)
{
	name = n;
	color = c;
}

void Object::setName(const string& n)
{
	name = n;
}


string Object::getName() const
{
	return name;
}

string Object::getColor() const
{
	return color;
}

void Object::setColor(const string& c)
{
	color = c;
}

string Object::unlock(const bool o)
{
	return 0;
}
string Object::examine() const
{
	return 0;
}
string Object::open()
{
	return 0;
}
string Object::useWith(Object& object)
{
	return 0;
}

Object::~Object()
{
}

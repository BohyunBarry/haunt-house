

#include "Vase.h"



Vase::Vase(string n, string c) : Object(n,c)
{
}

string Vase::unlock(bool o)
{
	return "Huh? How to do that?";
}

string Vase::examine() const
{
	return "This is a " + getName() + " Vase";
}

string Vase::open()
{
	return "This will not work, the Vase cannot be opened!!";
}

string Vase::useWith(Object& object)
{
	return "That won't work with that...";
}

string Vase::smash() const
{
	return "You smashed the vase, why would you try to do that!";
}

Vase::~Vase()
{
}



#pragma once
#include <string>

using std::string;
// this class defines how the file the game developer wants to use in the game by giving directory, assuming 20 lines for each room in the game
class FileRead
{
private:
	// will hold file path
	string directory;
public:
	FileRead(string dir);
	// reads out the data back to the program to display or use in other ways
	string readRoom(const int& r) const;
	~FileRead();
};


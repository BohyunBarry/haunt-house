

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <regex>
#include "Object.h"
#include "Door.h"
#include "Note.h"
#include "Key.h"
#include "Room.h"
#include "Vase.h"
#include "FileRead.h"
#include "Container.h"
#include "Weapon.h"
#include "Enemy.h"

using std::vector;
using std::cout;
using std::endl;
using std::cin;
using std::regex;
using std::ifstream;

vector<Room> createRooms(const string& dir)
{
	// here we create the rooms and every object state within them
	vector<Note> notes;
	vector<Key> keys;
	vector<Container> containers;
	vector<Door> doors;
	vector<Enemy> enemies;
	vector<Weapon> weapons;
	vector<Object> objects;
	vector<Room> rooms;

	// file to read the data in from and variables to store that data, taken from directory parameter
	ifstream in(dir);

	bool isOpen;
	int roomNumber, power;
	string name, color, description, junk;

	// file format must contain below if statements, the container items must be added before the container is, otherwise will lead to an empty container
	// room must be last to place all of the other items into the room


	if (in)
	{

		do
		{
			// reading in the next value to parse from the file
			getline(in, junk);

			// words here are key tags used by input file to interpret how to create the fully contents of each room
			if (junk == "--NEW ROOM--")
			{
				// create the room and reset all containers for the next room contents
				getline(in, name);
				getline(in, color);
				in >> roomNumber;
				getline(in, junk);
				rooms.push_back(Room(name, color, roomNumber, notes, keys, containers, doors, enemies, weapons));
				notes.clear();
				keys.clear();
				containers.clear();
				doors.clear();
				enemies.clear();
				weapons.clear();

			}
			else if (junk == "--CONTAINER--")
			{
				// create container for vector and clear objects vector for next container
				getline(in, name);
				getline(in, color);
				containers.push_back(Container(name, description, objects));
				objects.clear();
			}
			else if (junk == "--CONTAINER WEAPON--")
			{
				// create weapon for container
				getline(in, name);
				getline(in, color);
				in >> power;
				getline(in, junk);
				objects.push_back(Weapon(name, color, power));
			}
			else if (junk == "--CONTAINER KEY--")
			{
				// create key for container
				getline(in, name);
				getline(in, color);
				objects.push_back(Key(name, color));
			}
			else if (junk == "--CONTAINER NOTE--")
			{
				// create note for container
				getline(in, name);
				getline(in, color);
				getline(in, description);
				objects.push_back(Note(name, color, description));
			}
			else if (junk == "--DOOR--")
			{
				// create door for vector 
				getline(in, name);
				getline(in, color);
				in >> isOpen;
				getline(in, junk);
				doors.push_back(Door(name, color, isOpen));
			}
			else if (junk == "--WEAPON--")
			{
				// create weapon for vector
				getline(in, name);
				getline(in, color);
				in >> power;
				getline(in, junk);
				weapons.push_back(Weapon(name, color, power));
			}
			else if (junk == "--ENEMY--")
			{
				// create enemy for vector
				getline(in, name);
				getline(in, color);
				in >> power;
				getline(in, junk);
				enemies.push_back(Enemy(name, color, power));
			}
			else if (junk == "--KEY--")
			{
				// create key for vector
				getline(in, name);
				getline(in, color);
				keys.push_back(Key(name, color));
			}
			else if (junk == "--NOTE--")
			{
				// create note for vector
				getline(in, name);
				getline(in, color);
				getline(in, description);
				notes.push_back(Note(name, color, description));
			}
			else
			{
				getline(in, junk);
			}
		} while (!in.eof());

		// clean up the file after ourselves
		in.close();

	}
	else
	{
		cout << "Error loading file: please check directory entered..." << endl;
	}

	// finally return all rooms to the game
	return rooms;
}

string getRoomName(Room &current, string &input)
{
	// each door is checked exists and is not called no door in the list for the room object
	if (input == "move north")
	{
		if (current.hasDoor(0) && current.getDoorName(0) != "no door")
		{
			return current.getDoorName(0);
		}
	}
	else if (input == "move east")
	{
		if (current.hasDoor(1) && current.getDoorName(1) != "no door")
		{
			return current.getDoorName(1);
		}
	}
	else if (input == "move south")
	{
		if (current.hasDoor(2) && current.getDoorName(2) != "no door")
		{
			return current.getDoorName(2);
		}
	}
	else if (input == "move west")
	{
		if (current.hasDoor(3) && current.getDoorName(3) != "no door")
		{
			return current.getDoorName(3);
		}
	}
	// default statment for invalid door selection	
	return "not valid";

}


void roomChange(FileRead& file, Room& r)
{
	cout << file.readRoom(r.getRoomNumber()) << endl;
}

Room setRoom(vector<Room>& rooms, Room& current, string& loc)
{
	for (int i = 0; i < rooms.size(); i++)
	{
		if (rooms[i].getName() == current.getName())
		{
			rooms[i] = current;
		}
	}

	for (Room r : rooms)
	{
		if (r.getName() == loc) {
			return r;
		}
	}


}

int selectInventory(vector<Object>& inventory, string& item)
{

	for (int i = 0; i < inventory.size(); i++)
	{
		if (inventory[i].getName() == item)
		{
			return i;
		}
	}
	return -1;
}


// prints out names of all items in the inventory
void showInventory(vector<Object>& inventory)
{
	cout << "Your inventory Contains:\n" << endl;
	for (Object o : inventory)
	{
		cout << o.getName() << endl;
	}
}

// checks an item exists and returns true or false
bool checkInventory(const string& search, const vector<Object>& inventory)
{
	// returns true if object is found, otherwise player does not yet have the correct item
	for (Object o : inventory)
	{
		if (search == o.getName())
		{
			return true;
		}
	}
	return false;
}



// controls all examine commands
void examineObjects(string& input, Room& current, vector<Object>& inventory)
{
	// regex for examining different items
	regex rooms("main[ ]?hall|hall|bed[ ]?room|kitchen|bath[ ]?room|basement");
	regex keys("bath[ ]?room key|basement key|hall key");
	regex direction("north|east|south|west");


	// rooms selected
	if (regex_match(input.substr(8), rooms))
	{
		// checking to examine the room, name must match exactly to be the current room
		if (current.getName() == input.substr(8))
		{
			cout << current.examine() << endl;
		}
		else
		{
			cout << "Cannot examine this room called " << input.substr(8) << endl;
		}
	}

	// keys selected
	else if (regex_match(input.substr(8), keys))
	{
		// check for which key was looked for and check if in the correct room
		if (input.substr(8) == "bathroom key" || input.substr(8) == "bath room key")
		{
			// checks drawer is opened for the bedroom key to be in the room
			if (current.getName() == "bedroom" && current.getKeys().size() > 0)
			{
				vector<Key> keys = current.getKeys();
				cout << keys[0].examine() << endl;
			}
			else
			{
				cout << "There is no bathroom key here" << endl;
			}
		}
		else if (input.substr(8) == "basement key")
		{

			if (current.getName() == "bathroom")
			{
				vector<Key> keys = current.getKeys();
				cout << keys[0].examine() << endl;
			}
			else
			{
				cout << "There is no basement key here" << endl;
			}
		}
		else if (input.substr(8) == "hall key")
		{
			// checks if the ming vase container has already been broken for the key to be in the room
			if (current.getName() == "basement" && current.getKeys().size() > 0)
			{
				vector<Key> keys = current.getKeys();
				cout << keys[0].examine() << endl;
			}
			else
			{
				cout << "There is no hall key here" << endl;
			}
		}
		else if (checkInventory(input.substr(8), inventory));
		{

		}
	}
	//container selected - for basement, enemy bear must be dead
	else if (input.substr(8) == "ming vase" || input.substr(8) == "drawer")
	{
		if (current.getEnemies().size() == 0)
		{
			// checks correct room and container name as well as container has not been opened yet otherwise is gone
			if ((current.getName() == "bedroom" && input.substr(8) == "drawer" && current.getContainers().size() > 0) || (current.getName() == "basement" && input.substr(8) == "ming vase" && current.getContainers().size() > 0))
			{
				vector<Container> containers = current.getContainers();
				cout << containers[0].examine() << endl;
			}
			else
			{
				cout << "There is no " << input.substr(8) << " in this room to examine ..." << endl;
			}
		}
		else
		{
			cout << "The Wild Bear swings viciously at you, maybe you should deal with the bear first..." << endl;
		}


	}
	//enemy selected
	else if (input.substr(8) == "wild bear" || input.substr(8) == "bear")
	{
		vector<Enemy> enemies = current.getEnemies();
		enemies[0].examine();
	}
	// door location selected
	else if (regex_match(input.substr(8), direction))
	{
		vector<Door> doors = current.getDoors();
		if (input.substr(8) == "north" && !(doors[0].getName() == "no door"))
		{
			cout << doors[0].examine() << endl;
		}
		else if (input.substr(8) == "east" && !(doors[1].getName() == "no door"))
		{
			cout << doors[1].examine() << endl;
		}
		else if (input.substr(8) == "south" && !(doors[2].getName() == "no door"))
		{
			cout << doors[2].examine() << endl;
		}
		else if (input.substr(8) == "west" && !(doors[3].getName() == "no door"))
		{
			cout << doors[3].examine() << endl;
		}
		else
		{
			cout << "There is just a blank wall, how to examine no door?" << endl;
		}
	}
	// examine notes
	else if ((input.substr(8) == "leaflet" || input.substr(8) == "riddle") && current.getNotes().size() > 0)
	{
		// check to confirm are in the right room and the text matches the right note name
		if ((current.getName() == "hall" && input.substr(8) == "leaflet") || (current.getName() == "bathroom" && input.substr(8) == "riddle"))
		{
			vector<Note> notes = current.getNotes();
			cout << notes[0].examine() << endl;
		}
		else
		{
			cout << "There is no " << input.substr(8) << " here" << endl;
		}
	}
	// examine keys
	else if (input.substr(8) == "basement key" || input.substr(8) == "bathroom key" || input.substr(8) == "bath room key" || input.substr(8) == "hall key")
	{
		// check to confirm are in the right room and the text matches the right key and the drawer is opened and check is not taken by player already
		if (current.getName() == "bedroom" && (input.substr(8) == "bathroom key" || input.substr(8) == "bath room key") && current.getContainers().size() == 0 && current.getKeys().size() > 0)
		{
			current.getKeys()[0].examine();
		}
		// basement key already revealed in this room for player to take
		else if (current.getName() == "bathroom" && input.substr(8) == "basement key"  && current.getKeys().size() > 0)
		{
			current.getKeys()[0].examine();
		}
		// ming vase has been destroyed to remove container in the room
		else if (current.getName() == "basement" && input.substr(8) == "hall key" && current.getContainers().size() == 0 && current.getKeys().size() > 0)
		{
			current.getKeys()[0].examine();
		}
		else
		{
			cout << "There is no " << input.substr(8) << " here" << endl;
		}
	}
	// examine Weapons
	else if (input.substr(8) == "gun" || input.substr(8) == "knife")
	{
		// check to confirm are in the right room and the text matches the gun and the drawer is opened, gun inside drawer with the key and hasn't been taken yet
		if (current.getName() == "bedroom" && input.substr(8) == "gun" && current.getContainers().size() == 0 && current.getWeapons().size() > 0)
		{
			cout << current.getWeapons()[0].examine() << endl;
		}
		// Checking for the knife in the kitchen and hasn't been taken yet
		else if (current.getName() == "kitchen" && input.substr(8) == "knife" && current.getWeapons().size() > 0)
		{
			cout << current.getWeapons()[0].examine() << endl;
		}
		else
		{
			cout << "There is no " << input.substr(8) << " here" << endl;
		}
	}
	// default for nothing to examine
	else
	{
		cout << "no item here to examine called " << input.substr(8) << endl;
	}
}




void takeObjects(string& input, Room& current, vector<Object>& inventory)
{
	regex rooms("main[ ]?hall|hall|bed[ ]?room|kitchen|bath[ ]?room|basement");

	if (regex_match(input, rooms))
	{
		cout << "You cannot take the room with you! You are inside the room..." << endl;
	}

	if (input.substr(5) == "basement key")
	{
		if (current.getName() == "bathroom")
		{
			inventory.push_back(current.getKeys()[0]);
			vector<Key> empty;
			current.setKeys(empty);
			cout << "You put the " << input.substr(5) << " in your inventory" << endl;
		}
		else
		{
			cout << "There is no basement key here" << endl;
		}

	}
	else if (input.substr(5) == "bathroom key" || input.substr(5) == "bath room key")
	{
		if (current.getName() == "bedroom" && current.getKeys().size() > 0)
		{
			inventory.push_back(current.getKeys()[0]);
			vector<Key> empty;
			current.setKeys(empty);
			cout << "You put the " << input.substr(5) << " in your inventory" << endl;
		}
		else
		{
			cout << "There is no " << input.substr(5) << " key here" << endl;
		}

	}
	else if (input.substr(5) == "hall key")
	{
		// only if the vase container has been destroyed by the player
		if (current.getName() == "basement" && current.getContainers().size() == 0)
		{
			inventory.push_back(current.getKeys()[0]);
			vector<Key> empty;
			current.setKeys(empty);
			cout << "You put the " << input.substr(5) << " in your inventory" << endl;
		}
		else
		{
			cout << "There is no hall key here" << endl;
		}
	}
	else if (input.substr(5) == "gun" || input.substr(5) == "knife")
	{
		// placing gun from bedroom into inventory and out of the room
		if (((input.substr(5) == "gun" && current.getName() == "bedroom") || (input.substr(5) == "knife" && current.getName() == "kitchen")) && current.getWeapons().size() > 0)
		{
			cout << "You put the " << input.substr(5) << " into your inventory" << endl;
			inventory.push_back(current.getWeapons()[0]);
			vector<Weapon> weapons;
			current.setWeapons(weapons);
		}
		else
		{
			cout << "There is no " << input.substr(5) << " here to take" << endl;
		}
	}
	else if (input.substr(5) == "leaflet" || input.substr(5) == "riddle")
	{
		if (((input.substr(5) == "leaflet" && current.getName() == "hall") || (input.substr(5) == "riddle" && current.getName() == "bathroom")) && current.getNotes().size() > 0)
		{
			cout << "You put the " << input.substr(5) << " into your inventory" << endl;
			inventory.push_back(current.getNotes()[0]);
			vector<Note> notes;
			current.setNotes(notes);
		}
	}
	// default for incorrect take command
	else
	{
		cout << "There is no item called " << input.substr(5) << " to take" << endl;
	}
}




void useObjects(string& input, Room& current, vector<Object>& inventory, bool& alive)
{
	// using keys
	if (input.substr(4, 12) == "bathroom key" || input.substr(4, 13) == "bath room key" || input.substr(4, 12) == "basement key" || input.substr(4, 8) == "hall key")
	{
		// check in right room, right door, right item and inventory contains it, for bathroom door
		if ((input.substr(4, 12) == "bathroom key" || input.substr(4, 13) == "bath room key") && input.find("on") && current.getName() == "kitchen")
		{
			// check for inventory contains item
			if (checkInventory("bathroom key", inventory) && input.substr(input.find("on") + 3) == "south")
			{
				// have to set the doors back into the room after changing the locked state
				vector<Door> doors = current.getDoors();
				cout << doors[2].unlock(true) << endl;
				current.setDoors(doors);
			}
			else if (checkInventory("bathroom key", inventory))
			{
				cout << "You try to use the key but it won't turn, maybe it's the wrong door for this key?" << endl;
			}
			else
			{
				cout << "You don't have the item to unlock this in your Inventory!" << endl;
			}
		}
		// check for basement door
		else if (input.substr(4, 12) == "basement key" && input.find("on") && current.getName() == "kitchen")
		{
			// check for inventory contains item
			if (checkInventory("basement key", inventory) && input.substr(input.find("on") + 3) == "north")
			{
				// have to set the doors back into the room after changing the locked state
				vector<Door> doors = current.getDoors();
				cout << doors[0].unlock(true) << endl;
				current.setDoors(doors);
			}
			else if (checkInventory("basement key", inventory))
			{
				cout << "You try to use the key but it won't turn, maybe it's the wrong door for this key?" << endl;
			}
			else
			{
				cout << "You don't have the item to unlock this in your Inventory!" << endl;
			}
		}
		// check for hall door
		else if (input.substr(4, 8) == "hall key" && input.find("on") && current.getName() == "hall")
		{
			// check for inventory contains item
			if (checkInventory("hall key", inventory) && input.substr(input.find("on") + 3) == "south")
			{
				// have to set the doors back into the room after changing the locked state
				vector<Door> doors = current.getDoors();
				cout << doors[2].unlock(true) << endl;
				current.setDoors(doors);
			}
			else if (checkInventory("hall key", inventory))
			{
				cout << "You try to use the key but it won't turn, maybe it's the wrong door for this key?" << endl;
			}
			else
			{
				cout << "You don't have the item to unlock this in your Inventory!" << endl;
			}
		}
	}
	//using weapons on enemies
	else if ((input.substr(4, 3) == "gun" || input.substr(4, 5) == "knife") && input.find_first_of('on'))
	{
		if (current.getName() == "basement" && (input.find("bear")) || input.find("wild bear"))
		{
			if (checkInventory("gun", inventory) && current.getEnemies().size() > 0 && input.substr(4, 3) == "gun")
			{
				cout << "You fire the gun at the bear, the bullet bounced off it's curved skull, enraged by the loud sound, the bear charges at you and kills you.. Should have read the 'riddle' more carefully..." << endl;
				alive = false;
			}
			else if (checkInventory("knife", inventory) && current.getEnemies().size() > 0 && input.substr(4, 5) == "knife")
			{
				cout << "You take out the knife and some strange power takes over you, running towards the bear, you feel like you were a wild hunter in the past you somehow forgot" << endl;
				cout << "taking the knife in your right hand, you jump around the bear as it moves to slash at your face again and drive the knife deep into it's neck. The bear chokes and dies, completely dead.." << endl;
				vector<Enemy> enemies;
				current.setEnemies(enemies);
			}
			else if (current.getEnemies().size() == 0)
			{
				cout << "The bear is already dead, you no longer need to wield this weapon for now..." << endl;
			}
			else
			{
				cout << "You don't have this" << input.substr(input.find_first_of(' ')) << endl;
			}

		}
		else
		{
			cout << "You cannot use the weapon like this!" << endl;
		}
	}
	// default for not correct word structure for using something
	else
	{
		cout << "You cannot use this like that..." << endl;
	}
}



// changes state of room, you need to use pointers in all functions to change the current rooms state
void openObjects(string& input, Room& current, vector<Object>& inventory)
{
	//opening notes
	if ((input.substr(5) == "leaflet" || input.substr(5) == "riddle") && current.getNotes().size() > 0)
	{
		if (current.getName() == "hall" || current.getName() == "bathroom")
		{
			cout << current.getNotes()[0].open() << endl;
		}
		else
		{
			cout << "There is no " << input.substr(5) << " here" << endl;
		}
	}
	// opening containers - destroys container to leave objects in the room - also smash ming vase here
	else if (input.substr(5) == "drawer" || input.substr(5) == "ming vase" || input.substr(6) == "ming vase" || input.substr(5) == "vase" || input.substr(6) == "vase")
	{

		// check container not already removed, matching room and name of the container is okay first
		if ((current.getName() == "bedroom" && input.substr(5) == "drawer" && current.getContainers().size() > 0) || (current.getName() == "basement" && input.substr(6) == "ming vase" && current.getContainers().size() > 0))
		{
			// There is no bear to interrupt the opening or breaking operations of the ming vase
			if (current.getEnemies().size() == 0)
			{
				// first get containers from the room, we only have one in certain rooms
				vector<Container> containers = current.getContainers();
				// open sets internal boolean to true when we get the contents
				containers[0].open();
				// store objects into the vector and get the only item, a key out.
				vector<Object> objects = containers[0].getContents();
				if (input.substr(6) == "ming vase")
				{
					cout << containers[0].smash() << endl;
					cout << "You open up the " << input.substr(6) << " revealing:\n" << endl;
				}
				else
				{
					cout << "You open up the " << input.substr(5) << " revealing:\n" << endl;
				}



				for (Object obj : objects)
				{
					cout << obj.getName() << endl;
				}

				Key* k = (Key*)&objects[0];
				Key key = (Key)*k;
				// create new vector with the key and place into the room.
				vector<Key> keys;
				keys.push_back(key);
				// validation testing was done here to check keys before and after container opened up
				current.setKeys(keys);
				// same for the knife in the bedroom
				if (current.getName() == "bedroom")
				{
					Weapon* w = (Weapon*)&objects[1];
					Weapon weapon = (Weapon)*w;
					vector<Weapon> weapons;
					weapons.push_back(weapon);
					current.setWeapons(weapons);
				}
				// technically destroy the container by emptying the vector and put back into the room, no longer a container
				containers.clear();
				current.setContainers(containers);
			}
			else
			{
				cout << "The wild bear makes a furious attempt to slash your face as you approach, try to deal with the bear first..." << endl;
			}

		}
		else if ((current.getName() == "basement" && input.substr(5) == "ming vase" && current.getContainers().size() > 0))
		{
			cout << "You cannot open the ming vase, it looks like to open it you have to 'smash' it..." << endl;
		}
		else
		{
			// does not exist or the container has already been opened and removed from the room
			cout << "There is no " << input.substr(5) << " in this room" << endl;
		}




	}
	// default for something not matching any words from above
	else
	{
		cout << "There is no " << input.substr(5) << " in this room" << endl;
	}
}
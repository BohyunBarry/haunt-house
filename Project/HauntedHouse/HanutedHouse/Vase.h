
#pragma once
#include "Object.h"
#include <string>

using std::string;
// defines what can be in a vase
class Vase : public Object
{
public:
	Vase(string n, string c);
	string unlock(const bool o);
	string examine() const;
	string open();
	string useWith(Object& object);
	string smash() const;
	~Vase();
};

